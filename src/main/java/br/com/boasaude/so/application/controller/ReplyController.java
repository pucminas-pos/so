package br.com.boasaude.so.application.controller;

import br.com.boasaude.so.domain.model.internal.Reply;
import br.com.boasaude.so.domain.service.ReplyService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("replies")
@RequiredArgsConstructor
public class ReplyController {

    private final ReplyService replyService;


    @PostMapping
    public ResponseEntity save(@RequestBody Reply reply) {
        return ResponseEntity.status(HttpStatus.CREATED).body(replyService.save(reply));
    }

}
