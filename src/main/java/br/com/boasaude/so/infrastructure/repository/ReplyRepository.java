package br.com.boasaude.so.infrastructure.repository;

import br.com.boasaude.so.domain.model.internal.Reply;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReplyRepository extends CrudRepository<Reply, Long> {
}
