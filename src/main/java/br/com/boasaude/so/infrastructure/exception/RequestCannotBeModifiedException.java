package br.com.boasaude.so.infrastructure.exception;

public class RequestCannotBeModifiedException extends RuntimeException {

    public RequestCannotBeModifiedException() {
    }

    public RequestCannotBeModifiedException(String message) {
        super(message);
    }

    public RequestCannotBeModifiedException(String message, Throwable cause) {
        super(message, cause);
    }

    public RequestCannotBeModifiedException(Throwable cause) {
        super(cause);
    }
}
