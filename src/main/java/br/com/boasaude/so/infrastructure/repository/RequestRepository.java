package br.com.boasaude.so.infrastructure.repository;

import br.com.boasaude.so.domain.model.internal.Request;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RequestRepository extends CrudRepository<Request, Long> {
}
