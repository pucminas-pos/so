package br.com.boasaude.so.domain.model.internal;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Builder
@Entity(name = "replies")
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Reply implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @NotBlank
    private String fullname;

    @NotBlank
    private String email;

    @NotBlank
    private String role;

    @NotBlank
    private String message;
    private String protocol;
    private LocalDateTime createdAt;

    @ManyToOne
    @JsonBackReference
    private Request request;

    @PrePersist
    public void prePersist() {
        this.protocol = UUID.randomUUID().toString();
        this.createdAt = LocalDateTime.now();
    }
}
