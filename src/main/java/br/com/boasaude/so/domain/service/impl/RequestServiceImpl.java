package br.com.boasaude.so.domain.service.impl;

import br.com.boasaude.so.domain.model.internal.Request;
import br.com.boasaude.so.domain.model.internal.RequestStatus;
import br.com.boasaude.so.domain.model.internal.Response;
import br.com.boasaude.so.domain.service.RequestService;
import br.com.boasaude.so.infrastructure.exception.RequestCannotBeModifiedException;
import br.com.boasaude.so.infrastructure.repository.RequestRepository;
import br.com.boasaude.so.infrastructure.exception.RequestNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class RequestServiceImpl implements RequestService {

    private final RequestRepository requestRepository;

    @Override
    public Request findById(Long id) {
        return requestRepository.findById(id).orElseThrow(() -> {
            throw new RequestNotFoundException("request not found");
        });
    }

    @Override
    public Request save(Request request) {
        return requestRepository.save(request);
    }

    @Override
    public Request changeStatus(Long id, RequestStatus status) {
        Request request = findById(id);
        if (request.getStatus() == RequestStatus.CLOSED)
            throw new RequestCannotBeModifiedException("request cannot be modified");

        request.setStatus(status);
        return requestRepository.save(request);
    }
}
