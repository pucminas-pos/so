package br.com.boasaude.so.domain.service;

import br.com.boasaude.so.domain.model.internal.Reply;
import br.com.boasaude.so.domain.model.internal.Request;
import br.com.boasaude.so.domain.model.internal.RequestStatus;
import br.com.boasaude.so.domain.model.internal.Response;

public interface ReplyService {

    Reply save(Reply reply);
}
