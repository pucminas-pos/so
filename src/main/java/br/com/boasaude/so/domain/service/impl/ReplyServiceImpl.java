package br.com.boasaude.so.domain.service.impl;

import br.com.boasaude.so.domain.model.internal.Reply;
import br.com.boasaude.so.domain.service.ReplyService;
import br.com.boasaude.so.infrastructure.repository.ReplyRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ReplyServiceImpl implements ReplyService {

    private final ReplyRepository replyRepository;


    @Override
    public Reply save(Reply reply) {
        return replyRepository.save(reply);
    }
}
