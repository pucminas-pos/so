package br.com.boasaude.so.domain.model.internal;

public enum RequestStatus {

    OPENED, IN_PROGRESS, CLOSED;

}
