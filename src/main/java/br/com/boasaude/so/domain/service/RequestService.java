package br.com.boasaude.so.domain.service;

import br.com.boasaude.so.domain.model.internal.Request;
import br.com.boasaude.so.domain.model.internal.RequestStatus;
import br.com.boasaude.so.domain.model.internal.Response;

public interface RequestService {

    Request save(Request request);

    Request changeStatus(Long id, RequestStatus status);

    Request findById(Long id);
}
