package br.com.boasaude.so.domain.model.internal;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Data
@Builder
@Entity(name = "requests")
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Request implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @NotBlank
    private String fullname;

    @NotBlank
    private String cpf;

    @NotBlank
    private String email;

    @NotBlank
    private String role;

    @NotBlank
    private String subject;

    @NotBlank
    private String message;

    @NotNull
    @Enumerated(EnumType.STRING)
    private RequestType type;

    private String protocol;
    private LocalDateTime createdAt;

    @Enumerated(EnumType.STRING)
    private RequestStatus status;

    @OrderBy("id DESC")
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name="request_id")
    @JsonManagedReference
    private List<Reply> replies;


    @PrePersist
    public void prePersist() {
        this.status = RequestStatus.OPENED;
        this.protocol = UUID.randomUUID().toString();
        this.createdAt = LocalDateTime.now();
    }
}
