FROM maven:3.6.0-jdk-11-slim AS build
COPY src /home/so/src
COPY pom.xml /home/so
RUN mvn -f /home/so/pom.xml clean package

FROM adoptopenjdk/openjdk11:alpine
COPY --from=build /home/so/target/so.jar /usr/local/lib/so.jar
EXPOSE 9002
ENTRYPOINT ["java","-jar","/usr/local/lib/so.jar"]